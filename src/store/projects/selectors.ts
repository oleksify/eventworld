// --- Types
import { State, Project } from '../../types'

/**
 * Functions
 */

export const getAllProjects = (state: State): Project[] => state.projects

export const selectProject = (state: State, id: number): Project | undefined =>
  state.projects?.filter(project => project.id === id)[0]
