// --- Types
import { Actions } from '../types'
import { ActionTypes } from './actions'
import { Project } from '../../types'

/**
 * Types
 */

type State = Project[]

/**
 * Reducer
 */

export function reducer(state: State = [], action: Actions): Project[] {
  switch (action.type) {
    case ActionTypes.GetProjects: {
      return action.payload
    }
    case ActionTypes.GetProject: {
      return [...state, action.payload]
    }
    case ActionTypes.AddProject: {
      return [...state, action.payload]
    }
    case ActionTypes.RemoveProject: {
      return state.filter(project => project.id !== action.payload)
    }
    case ActionTypes.UpdateProject: {
      return [
        ...state.filter(project => project.id !== action.payload.id),
        action.payload
      ]
    }
    default:
      return state
  }
}
