// --- Types
import { Project } from '../../types'
import { ActionsUnion, DispatchAction } from '../types'

// --- Utils
import { createAction } from '../action'
import { get, del, post } from '../../lib/http'

/**
 * Types
 */

export enum ActionTypes {
  GetProjects = '@@projects/GET_PROJECTS',
  GetProject = '@@projects/GET_PROJECT',
  AddProject = '@@projects/ADD_PROJECT',
  RemoveProject = '@@projects/REMOVE_PROJECT',
  UpdateProject = '@@projects/UPDATE_PROJECT'
}

export const Actions = {
  getProjects: (options: Project[]) =>
    createAction(ActionTypes.GetProjects, options),
  addProject: (options: Project) =>
    createAction(ActionTypes.AddProject, options),
  removeProject: (id: number) => createAction(ActionTypes.RemoveProject, id),
  getProject: (options: Project) =>
    createAction(ActionTypes.GetProject, options),
  updateProject: (options: Project) =>
    createAction(ActionTypes.UpdateProject, options)
}

export type Actions = ActionsUnion<typeof Actions>

/**
 * Functions
 */

export function getProjects(): DispatchAction {
  return async dispatch => {
    const response = await get<Project[]>('/projects')

    if (response.ok) {
      dispatch(Actions.getProjects(response.parsedBody as Project[]))
    } else {
      console.error(response.error)
    }
  }
}

export function getProject(id: number): DispatchAction {
  return async dispatch => {
    const response = await get<Project | undefined>(`/projects/${id}`)

    if (response.ok) {
      dispatch(Actions.getProject(response.parsedBody as Project))
    } else {
      console.error(response.error)
    }
  }
}

export function removeProject(id: number): DispatchAction {
  return async dispatch => {
    const response = await del<{}>(`/projects/${id}`)

    if (response.ok) {
      dispatch(Actions.removeProject(id))
    } else {
      console.error(response.error)
    }
  }
}

export function addProject(options: Omit<Project, 'id'>): DispatchAction {
  return async dispatch => {
    const response = await post<Project>('/projects', options)

    if (response.ok) {
      dispatch(Actions.addProject(response.parsedBody as Project))
    } else {
      console.error(response.error)
    }
  }
}
