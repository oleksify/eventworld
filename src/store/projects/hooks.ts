// --- Dependencies
import { useSelector } from 'react-redux'

// --- Selectors
import { getAllProjects } from './selectors'

export const useProjects = () => useSelector(getAllProjects)
