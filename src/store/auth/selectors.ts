// --- Dependencies
import { createSelector } from 'reselect'

// --- Types
import { State, Auth, User } from '../../types'

/**
 * Functions
 */

export const getAuth = (state: State): Auth => state.auth

export const getCurrentUser = (state: State): User | undefined =>
  state.auth.user

export const isLoggedIn = createSelector(
  getAuth,
  state => state.isAuthenticated
)

export const getError = createSelector(getAuth, state => state.error)
