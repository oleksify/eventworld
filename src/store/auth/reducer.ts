// --- Types
import { Actions } from '../types'
import { ActionTypes } from './actions'
import { Auth } from '../../types'

// --- Constants
import { TOKEN_KEY } from './actions'

/**
 * Types
 */

type State = Auth

/**
 * Reducer
 */

export function reducer(
  state: State = {
    isAuthenticated: localStorage.getItem(TOKEN_KEY) ? true : false
  },
  action: Actions
) {
  switch (action.type) {
    case ActionTypes.Login: {
      return action.payload
    }
    case ActionTypes.SignUp: {
      return action.payload
    }
    case ActionTypes.Logout: {
      return action.payload
    }
    case ActionTypes.SetError: {
      return action.payload
    }
    case ActionTypes.SetCurrentUser: {
      return action.payload
    }
    default:
      return state
  }
}
