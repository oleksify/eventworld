// --- Dependencies
import { useSelector } from 'react-redux'

// --- Selectors
import { isLoggedIn, getError, getCurrentUser } from './selectors'

export const useLoggedIn = () => useSelector(isLoggedIn)
export const useCurrentUser = () => useSelector(getCurrentUser)
export const useAuthErrors = () => useSelector(getError)
