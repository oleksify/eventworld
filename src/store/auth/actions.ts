// --- Dependencies
import jwtDecode from 'jwt-decode'

// --- Types
import { ActionsUnion, DispatchAction } from '../types'
import { Auth, User } from '../../types'

// --- Utils
import { createAction } from '../action'
import { post, get } from '../../lib/http'
import history from '../../lib/history'

/**
 * Types
 */

interface LoginParams {
  email: string
  password: string
}

interface SignUpParams extends LoginParams {
  name: string
}

interface ApiResponse {
  accessToken?: string
  error?: string
}

interface Token {
  email: string
  exp: number
  iat: number
  sub: string
}

export enum ActionTypes {
  Login = '@@auth/LOGIN',
  Logout = '@@auth/LOGOUT',
  SignUp = '@@auth/SIGN_UP',
  SetError = '@@auth/SET_ERROR',
  SetCurrentUser = '@@auth/SET_CURRENT_USER'
}

export const Actions = {
  login: (options: Auth) => createAction(ActionTypes.Login, options),
  logout: (options: Auth) => createAction(ActionTypes.Logout, options),
  signUp: (options: Auth) => createAction(ActionTypes.SignUp, options),
  setError: (options: Auth) => createAction(ActionTypes.SetError, options),
  setCurrentUser: (options: Auth) =>
    createAction(ActionTypes.SetCurrentUser, options)
}

export type Actions = ActionsUnion<typeof Actions>

/**
 * Helpers
 */

export const TOKEN_KEY = '__ew_token'

/**
 * Actions
 */

export function login(options: LoginParams): DispatchAction {
  return async dispatch => {
    // Login
    const response = await post<ApiResponse>('/login', options)

    if (response.ok) {
      localStorage.setItem(
        TOKEN_KEY,
        response.parsedBody?.accessToken as string
      )
      const userData: Token = jwtDecode(
        localStorage.getItem(TOKEN_KEY) as string
      )
      const userResponse = await get<User>(`/users/${userData.sub}`)
      dispatch(
        Actions.login({
          isAuthenticated: true,
          error: response.error,
          user: userResponse.parsedBody
        })
      )

      // Redirect to dashboard
      history.push('/admin/dashboard')
    } else {
      dispatch(
        Actions.setError({
          error: response.error,
          isAuthenticated: false,
          user: undefined
        })
      )
    }
  }
}

export function logout(): DispatchAction {
  return async dispatch => {
    localStorage.removeItem(TOKEN_KEY)
    dispatch(
      Actions.logout({
        isAuthenticated: false,
        error: undefined,
        user: undefined
      })
    )
  }
}

export function signUp(options: SignUpParams): DispatchAction {
  return async dispatch => {
    // Sign up
    const response = await post<ApiResponse>('/register', options)

    if (response.ok) {
      localStorage.setItem(
        TOKEN_KEY,
        response.parsedBody?.accessToken as string
      )
      const userData: Token = jwtDecode(
        localStorage.getItem(TOKEN_KEY) as string
      )
      const userResponse = await get<User>(`/users/${userData.sub}`)
      dispatch(
        Actions.signUp({
          isAuthenticated: true,
          error: response.error,
          user: userResponse.parsedBody
        })
      )

      // Redirect to dashboard
      history.push('/admin/dashboard')
    } else {
      dispatch(
        Actions.setError({
          error: response.error,
          isAuthenticated: false
        })
      )
    }
  }
}

export function setCurrentUser(): DispatchAction {
  return async dispatch => {
    const userData: Token = jwtDecode(localStorage.getItem(TOKEN_KEY) as string)
    const userResponse = await get<User>(`/users/${userData.sub}`)

    if (userResponse.ok) {
      dispatch(
        Actions.setCurrentUser({
          isAuthenticated: true,
          error: userResponse.error,
          user: userResponse.parsedBody
        })
      )
    } else {
      Actions.setError({
        error: userResponse.error,
        isAuthenticated: false,
        user: undefined
      })
    }
  }
}
