// --- Dependencies
import * as React from 'react'
import { applyMiddleware, compose, createStore } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'

// --- Utils
import { reducer } from './reducer'

// --- Types
import { Store } from './types'

/**
 * Helpers
 */

const composeEnhancers =
  process.env.NODE_ENV === 'development'
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    : compose

const store: Store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(thunkMiddleware))
)

/**
 * Component
 */

export const StoreProvider: React.FC = ({ children }) => (
  <Provider store={store}>{children}</Provider>
)
