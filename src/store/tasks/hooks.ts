// --- Dependencies
import { useSelector } from 'react-redux'

// --- Selectors
import { getProjectTasks } from './selectors'

// --- Types
import { State } from '../../types'

export const useTasks = (projectId: string) =>
  useSelector((state: State) => getProjectTasks(state, projectId))
