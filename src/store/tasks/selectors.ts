// --- Dependencies
import { createSelector } from 'reselect'

// --- Types
import { State } from '../../types'

/**
 * Functions
 */

export const getProjectTasks = createSelector(
  (state: State) => state.tasks,
  (_state: State, projectId: string) => projectId,
  (tasks, projectId) => tasks.filter(task => task.projectId === projectId)
)
