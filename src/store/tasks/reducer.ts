// --- Types
import { Actions } from '../types'
import { ActionTypes } from './actions'
import { Task } from '../../types'

/**
 * Types
 */

type State = Task[]

/**
 * Reducer
 */

export function reducer(state: State = [], action: Actions): Task[] {
  switch (action.type) {
    case ActionTypes.GetTasks: {
      return action.payload
    }
    case ActionTypes.AddTask: {
      return [...state, action.payload]
    }
    case ActionTypes.RemoveTask: {
      return state.filter(task => task.id !== action.payload)
    }
    case ActionTypes.UpdateTask: {
      return [
        ...state.filter(task => task.id !== action.payload.id),
        action.payload
      ]
    }
    default:
      return state
  }
}
