// --- Types
import { Task } from '../../types'
import { ActionsUnion, DispatchAction } from '../types'

// --- Utils
import { createAction } from '../action'
import { get, del, post } from '../../lib/http'

/**
 * Types
 */

export enum ActionTypes {
  GetTasks = '@@tasks/GET_TASKS',
  AddTask = '@@tasks/ADD_TASK',
  RemoveTask = '@@tasks/REMOVE_TASK',
  UpdateTask = '@@tasks/UPDATE_TASK'
}

export const Actions = {
  getTasks: (options: Task[]) => createAction(ActionTypes.GetTasks, options),
  addTask: (options: Task) => createAction(ActionTypes.AddTask, options),
  removeTask: (id: number) => createAction(ActionTypes.RemoveTask, id),
  updateTask: (options: Task) => createAction(ActionTypes.UpdateTask, options)
}

export type Actions = ActionsUnion<typeof Actions>

/**
 * Functions
 */

export function getTasks(): DispatchAction {
  return async dispatch => {
    const response = await get<Task[]>('/tasks')

    if (response.ok) {
      dispatch(Actions.getTasks(response.parsedBody as Task[]))
    } else {
      console.error(response.error)
    }
  }
}

export function removeTask(id: number): DispatchAction {
  return async dispatch => {
    const response = await del<{}>(`/tasks/${id}`)

    if (response.ok) {
      dispatch(Actions.removeTask(id))
    } else {
      console.error(response.error)
    }
  }
}

export function addTask(options: Omit<Task, 'id'>): DispatchAction {
  return async dispatch => {
    const response = await post<Task>(
      `/projects/${options.projectId}/tasks`,
      options
    )

    if (response.ok) {
      dispatch(Actions.addTask(response.parsedBody as Task))
    } else {
      console.error(response.error)
    }
  }
}
