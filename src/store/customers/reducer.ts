// --- Types
import { Actions } from '../types'
import { ActionTypes } from './actions'
import { Customer } from '../../types'

/**
 * Types
 */

type State = Customer[]

/**
 * Reducer
 */

export function reducer(state: State = [], action: Actions): Customer[] {
  switch (action.type) {
    case ActionTypes.GetCustomers: {
      return action.payload
    }
    case ActionTypes.AddCustomer: {
      return [...state, action.payload]
    }
    case ActionTypes.RemoveCustomer: {
      return state.filter(customer => customer.id !== action.payload)
    }
    case ActionTypes.UpdateCustomer: {
      return [
        ...state.filter(customer => customer.id !== action.payload.id),
        action.payload
      ]
    }
    default:
      return state
  }
}
