// --- Types
import { State, Customer } from '../../types'

/**
 * Functions
 */

export const getAllCustomers = (state: State): Customer[] => state.customers

export const selectCustomer = (
  state: State,
  id: number
): Customer | undefined =>
  state.customers?.filter(customer => customer.id === id)[0]
