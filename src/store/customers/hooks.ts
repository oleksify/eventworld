// --- Dependencies
import { useSelector } from 'react-redux'

// --- Selectors
import { getAllCustomers } from './selectors'

export const useCustomers = () => useSelector(getAllCustomers)
