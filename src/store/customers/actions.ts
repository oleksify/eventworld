// --- Types
import { Customer } from '../../types'
import { ActionsUnion, DispatchAction } from '../types'

// --- Utils
import { createAction } from '../action'
import { get, del } from '../../lib/http'

/**
 * Types
 */

export enum ActionTypes {
  GetCustomers = '@@customers/GET_CUSTOMERS',
  AddCustomer = '@@customers/ADD_CUSTOMER',
  RemoveCustomer = '@@customers/REMOVE_CUSTOMER',
  UpdateCustomer = '@@customers/UPDATE_CUSTOMER'
}

export const Actions = {
  getCustomers: (options: Customer[]) =>
    createAction(ActionTypes.GetCustomers, options),
  addCustomer: (options: Customer) =>
    createAction(ActionTypes.AddCustomer, options),
  removeCustomer: (id: number) => createAction(ActionTypes.RemoveCustomer, id),
  updateCustomer: (options: Customer) =>
    createAction(ActionTypes.UpdateCustomer, options)
}

export type Actions = ActionsUnion<typeof Actions>

/**
 * Functions
 */

export function getCustomers(): DispatchAction {
  return async dispatch => {
    const response = await get<Customer[]>('/customers')

    if (response.ok) {
      dispatch(Actions.getCustomers(response.parsedBody as Customer[]))
    } else {
      console.error(response.error)
    }
  }
}

export function removeCustomer(id: number): DispatchAction {
  return async dispatch => {
    const response = await del<Customer[]>(`/customers/${id}`)

    if (response.ok) {
      dispatch(Actions.removeCustomer(id))
    } else {
      console.error(response.error)
    }
  }
}

export function addCustomer(options: Customer): DispatchAction {
  return async dispatch => {
    dispatch(Actions.addCustomer(options))
  }
}
