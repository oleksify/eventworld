// --- Dependencies
import { useSelector } from 'react-redux'

// --- Selectors
import { getAllUsers, selectUser } from './selectors'

// --- Types
import { State } from '../../types'

export const useUsers = () => useSelector(getAllUsers)

export const useUser = (email: string) =>
  useSelector((state: State) => selectUser(state, email))
