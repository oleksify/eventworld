// --- Types
import { Actions } from '../types'
import { ActionTypes } from './actions'
import { User } from '../../types'

/**
 * Types
 */

type State = User[]

/**
 * Reducer
 */

export function reducer(state: State = [], action: Actions): User[] {
  switch (action.type) {
    case ActionTypes.GetUsers: {
      return action.payload
    }
    case ActionTypes.AddUser: {
      return [...state, action.payload]
    }
    case ActionTypes.RemoveUser: {
      return state.filter(user => user.id !== action.payload)
    }
    case ActionTypes.UpdateUser: {
      return [
        ...state.filter(user => user.id !== action.payload.id),
        action.payload
      ]
    }
    default:
      return state
  }
}
