// --- Types
import { State, User } from '../../types'

/**
 * Functions
 */

export const getAllUsers = (state: State): User[] => state.users

export const selectUser = (state: State, email: string): User | undefined =>
  state.users?.filter(user => user.email === email)[0]
