// --- Types
import { User } from '../../types'
import { ActionsUnion, DispatchAction } from '../types'

// --- Utils
import { createAction } from '../action'
import { get, del } from '../../lib/http'

/**
 * Types
 */

export enum ActionTypes {
  GetUsers = '@@users/GET_USERS',
  AddUser = '@@users/ADD_USER',
  RemoveUser = '@@users/REMOVE_USER',
  UpdateUser = '@@users/UPDATE_USER'
}

export const Actions = {
  getUsers: (options: User[]) => createAction(ActionTypes.GetUsers, options),
  addUser: (options: User) => createAction(ActionTypes.AddUser, options),
  removeUser: (id: number) => createAction(ActionTypes.RemoveUser, id),
  updateUser: (options: User) => createAction(ActionTypes.UpdateUser, options)
}

export type Actions = ActionsUnion<typeof Actions>

/**
 * Functions
 */

export function getUsers(): DispatchAction {
  return async dispatch => {
    const response = await get<User[]>('/users')

    if (response.ok) {
      dispatch(Actions.getUsers(response.parsedBody as User[]))
    } else {
      console.error(response.error)
    }
  }
}

export function removeUser(id: number): DispatchAction {
  return async dispatch => {
    const response = await del<User[]>(`/users/${id}`)

    if (response.ok) {
      dispatch(Actions.removeUser(id))
    } else {
      console.error(response.error)
    }
  }
}

export function addUser(options: User): DispatchAction {
  return async dispatch => {
    dispatch(Actions.addUser(options))
  }
}
