// --- Dependencies
import { combineReducers, Reducer } from 'redux'

// --- Types
import { Actions } from './types'
import { State } from '../types'

// --- Reducers
import { reducer as projects } from './projects'
import { reducer as customers } from './customers'
import { reducer as users } from './users'
import { reducer as tasks } from './tasks'
import { reducer as auth } from './auth'

/**
 * Functions
 */

export const reducer: Reducer<State, Actions> = combineReducers({
  projects,
  customers,
  users,
  tasks,
  auth
})
