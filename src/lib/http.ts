/**
 * Types
 */
export interface IHttpResponse<T> extends Response {
  parsedBody?: T
  error?: string
}

/**
 * Functions
 */

export const getPath = (path: string) =>
  `${process.env.REACT_APP_API_URL}${path}`

export const http = <T>(request: RequestInfo): Promise<IHttpResponse<T>> => {
  return new Promise((resolve, _reject) => {
    let response: IHttpResponse<T>

    fetch(request)
      .then(res => {
        response = res
        return res.json()
      })
      .then(body => {
        if (response.ok) {
          response.parsedBody = body
          resolve(response)
        } else {
          throw body
        }
      })
      .catch(error => {
        response.error = error
        resolve(response)
      })
  })
}

export const get = async <T>(
  path: string,
  args: RequestInit = {
    method: 'get'
  }
): Promise<IHttpResponse<T>> => {
  return await http<T>(new Request(getPath(path), args))
}

export const post = async <T>(
  path: string,
  body: any,
  args: RequestInit = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }
): Promise<IHttpResponse<T>> => {
  return await http<T>(new Request(getPath(path), args))
}

export const put = async <T>(
  path: string,
  body: any,
  args: RequestInit = {
    method: 'put',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }
): Promise<IHttpResponse<T>> => {
  return await http<T>(new Request(getPath(path), args))
}

export const del = async <T>(
  path: string,
  args: RequestInit = {
    method: 'delete'
  }
): Promise<IHttpResponse<T>> => {
  return await http<T>(new Request(getPath(path), args))
}
