// --- Dependencies
import * as React from 'react'
import { Router, Switch, Route } from 'react-router-dom'

// --- Components
import PrivateRoute from './components/shared/PrivateRoute'

// --- Pages
import Home from './components/pages/site/Home'
import About from './components/pages/site/About'
import Login from './components/pages/site/Login'
import SignUp from './components/pages/site/SignUp'
import Dashboard from './components/pages/admin/Dashboard'
import Profile from './components/pages/admin/Profile'
import Projects from './components/pages/admin/Projects'
import Project from './components/pages/admin/Project'
import Settings from './components/pages/admin/Settings'
import Users from './components/pages/admin/Users'
import Customers from './components/pages/admin/Customers'

// --- Utils
import history from './lib/history'
import { StoreProvider } from './store'

// --- Styles
import 'tachyons'
import './assets/css/typography.css'

/**
 * Component
 */

const App: React.FC = () => {
  return (
    <StoreProvider>
      <div className="fira-code">
        <Router history={history}>
          <Switch>
            {/* Site Section */}

            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/signup">
              <SignUp />
            </Route>

            {/* Admin Section */}

            <PrivateRoute path="/admin">
              <Switch>
                <PrivateRoute path="/admin/dashboard">
                  <Dashboard />
                </PrivateRoute>

                <PrivateRoute exact path="/admin/projects">
                  <Projects />
                </PrivateRoute>

                <PrivateRoute exact path="/admin/projects/:id">
                  <Project />
                </PrivateRoute>

                <PrivateRoute path="/admin/customers">
                  <Customers />
                </PrivateRoute>

                <PrivateRoute path="/admin/users">
                  <Users />
                </PrivateRoute>

                <PrivateRoute path="/admin/settings">
                  <Settings />
                </PrivateRoute>

                <PrivateRoute path="/admin/profile">
                  <Profile />
                </PrivateRoute>
              </Switch>
            </PrivateRoute>
          </Switch>
        </Router>
      </div>
    </StoreProvider>
  )
}

export default App
