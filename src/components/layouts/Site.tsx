// --- Dependencies
import * as React from 'react'

// --- Components
import Header from '../shared/Header'

/**
 * Component
 */

const Site: React.FC = ({ children }) => (
  <div className="flex flex-column min-vh-100 h-100 dark-gray">
    <Header />
    <div className="flex flex-auto">
      <main className="flex-auto ma4">{children}</main>
    </div>
  </div>
)

export default Site
