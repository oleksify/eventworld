// --- Dependencies
import * as React from 'react'
import { useDispatch } from 'react-redux'

// --- Components
import AdminHeader from '../shared/AdminHeader'
import Sidebar from '../shared/Sidebar'

// --- Types
import { Dispatch } from '../../store/types'

// --- Actions & Hooks
import { setCurrentUser, useCurrentUser } from '../../store/auth'
import { useLoggedIn } from '../../store/auth'

/**
 * Types
 */

interface Props {
  pageTitle?: string
  children: React.ReactNode
  actions?: React.ReactNode
}

interface Token {
  email: string
  exp: number
  iat: number
  sub: string
}

/**
 * Component
 */

const Admin: React.FC<Props> = ({ pageTitle, children, actions }) => {
  const dispatch = useDispatch<Dispatch>()
  const isLoggedIn = useLoggedIn()
  const user = useCurrentUser()

  React.useEffect(() => {
    if (isLoggedIn && !user) {
      dispatch(setCurrentUser())
    }
  })

  return (
    <div className="flex flex-column min-vh-100 h-100 dark-gray">
      <div className="flex">
        <Sidebar user={user} />
        <div className="flex flex-column w-100">
          <AdminHeader />
          <div className="flex flex-auto flex-column bg-light-gray pa4">
            <div className="flex items-center justify-between">
              {pageTitle && (
                <h3 className="f3 mv0 fw5 dark-blue">{pageTitle}</h3>
              )}
              {actions && <div className="flex items-center">{actions}</div>}
            </div>
            <main className="flex-auto mt3">{children}</main>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Admin
