// --- Dependencies
import * as React from 'react'

/**
 * Component
 */

const Button: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = ({
  children,
  ...rest
}) => (
  <button
    className="bg-animate ttl flex items-center bn br2 lh-copy pv2 ph4 bg-dark-blue white f5 fw4 pointer hover-bg-blue"
    {...rest}
  >
    {children}
  </button>
)

export default Button
