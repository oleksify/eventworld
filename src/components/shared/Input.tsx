// --- Dependencies
import * as React from 'react'

/**
 * Component
 */

const Input: React.FC<React.InputHTMLAttributes<HTMLInputElement>> = props => (
  <input className="pv2 ph3 lh-copy f5 ba br2 b--moon-gray" {...props} />
)

export default Input
