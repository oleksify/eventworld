// --- Dependencies
import * as React from 'react'

/**
 * Component
 */

const Label: React.FC<React.LabelHTMLAttributes<HTMLLabelElement>> = ({
  children,
  ...rest
}) => (
  <label className="db f6 dark-gray pointer mb1 fw5 ttl" {...rest}>
    {children}
  </label>
)

export default Label
