// --- Dependencies
import * as React from 'react'
import { NavLink as RLink, NavLinkProps } from 'react-router-dom'

/**
 * Component
 */

const Link: React.FC<NavLinkProps & { customClasses?: string }> = ({
  children,
  className,
  customClasses,
  ...rest
}) => {
  return (
    <RLink
      {...rest}
      exact
      className={
        customClasses || `${className} hover-near-black gray link dib ttl f5`
      }
    >
      {children}
    </RLink>
  )
}

export default Link
