// --- Dependencies
import * as React from 'react'
import { Link } from 'react-router-dom'

// --- Components
import MainNav from '../shared/MainNav'

/**
 * Component
 */

const Header: React.FC = () => (
  <header className="ph4 flex justify-between items-center">
    <Link to="/" className="near-black link">
      <hgroup>
        <h1 className="mv0 fw6 flex items-center">
          CS<span className="gray fw4 f4 dib">::</span>WORLD
        </h1>
        <h2 className="f7 tracked ml1 gray ttu mv0 fw5">
          Best{' '}
          <abbr title="Computer Science" className="near-black fw7">
            CS
          </abbr>{' '}
          Project Management Tool
        </h2>
      </hgroup>
    </Link>

    <MainNav />
  </header>
)

export default Header
