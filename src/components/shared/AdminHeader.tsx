// --- Dependencies
import * as React from 'react'

// --- Components
import LogoutButton from '../elements/LogoutButton'
import Search from '../shared/Search'

/**
 * Component
 */

const Header: React.FC = () => {
  return (
    <header className="ph4 pv2 flex justify-between items-center bb b--light-gray">
      <div className="w-100">
        <Search />
      </div>

      <LogoutButton />
    </header>
  )
}

export default Header
