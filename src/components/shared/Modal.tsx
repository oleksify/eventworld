// --- Dependencies
import * as React from 'react'
import ReactModal from 'react-modal'

// --- Icons
import { ReactComponent as IconClose } from 'feather-icons/dist/icons/x-circle.svg'

/**
 * Types
 */

interface Props {
  title: string
  isOpen: boolean
  closeFn: () => void
  children: React.ReactNode
}

/**
 * Component
 */

const Modal: React.FC<Props> = ({ title, isOpen, closeFn, children }) => (
  <ReactModal
    isOpen={isOpen}
    onRequestClose={closeFn}
    className="pa4 fira-code bg-white top-2 left-2 right-2 absolute br2"
    style={{
      overlay: {
        backgroundColor: 'rgba(0,0,0,0.7)'
      }
    }}
  >
    <div className="flex justify-between pb3 mb3 bb b--light-gray">
      <h3 className="mv0 f4">{title}</h3>
      <button
        className="bg-transparent pa0 pointer bn"
        onClick={() => closeFn()}
      >
        <IconClose />
      </button>
    </div>
    <div>{children}</div>
  </ReactModal>
)

export default Modal
