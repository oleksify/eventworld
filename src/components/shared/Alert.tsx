// --- Dependencies
import * as React from 'react'

/**
 * Types
 */

export enum AlertType {
  Error,
  Info
}

interface IProps {
  type: AlertType
}

/**
 * Helpers
 */

const getTypeStyles = (type: AlertType) => {
  switch (type) {
    case AlertType.Error:
      return 'bg-red white'
    case AlertType.Info:
      return 'bg-light-gray dark-gray'
    default:
      return 'dark-gray ba b--light-gray'
  }
}

/**
 * Component
 */

const Alert: React.FC<IProps> = ({ type, children }) => (
  <div className={`pa3 br1 ${getTypeStyles(type)}`}>{children}</div>
)

export default Alert
