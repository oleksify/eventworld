// --- Dependencies
import * as React from 'react'
import Avatar from 'react-avatar'
import { NavLink } from 'react-router-dom'

// --- Components
import Link from './SidebarLink'

// --- Icons
import { ReactComponent as IconHome } from 'feather-icons/dist/icons/home.svg'
import { ReactComponent as IconProjects } from 'feather-icons/dist/icons/archive.svg'
import { ReactComponent as IconCustomers } from 'feather-icons/dist/icons/book.svg'
import { ReactComponent as IconUsers } from 'feather-icons/dist/icons/users.svg'

// --- Types
import { User } from '../../types'

/**
 * Types
 */

interface Props {
  user?: User
  isOpen?: boolean
}

/**
 * Component
 */

const Sidebar: React.FC<Props> = ({ isOpen = false, user }) => {
  return (
    <div className="bg-navy min-vh-100 flex flex-column justify-between pt2">
      <nav>
        <Link to="/admin/dashboard" icon={<IconHome />}>
          Dashboard
        </Link>
        <Link to="/admin/projects" icon={<IconProjects />}>
          Projects
        </Link>
        <Link to="/admin/customers" icon={<IconCustomers />}>
          Customers
        </Link>
        <Link to="/admin/users" icon={<IconUsers />}>
          Users
        </Link>
      </nav>
      {user && (
        <NavLink to="/admin/profile">
          <div className="tc mb4">
            <Avatar
              style={{
                border: '2px solid white',
                width: '40px',
                height: '40px'
              }}
              email={user?.email}
              name={user?.name}
              size="36"
              round
            />
            <div className="ml2 f7 nowrap dn">
              <div className="lh-copy">{user?.name}</div>
              <div className="lh-copy">{user?.email}</div>
            </div>
          </div>
        </NavLink>
      )}
    </div>
  )
}

export default Sidebar
