// --- Dependencies
import * as React from 'react'

// --- Icons
import { ReactComponent as IconSearch } from 'feather-icons/dist/icons/search.svg'

/**
 * Component
 */

const Search: React.FC = () => {
  return (
    <form className="w-100">
      <div className="relative w-100 pv1">
        <IconSearch className="absolute ml1 gray mt2" width={22} height={22} />
        <input
          placeholder="Enter keywords to start searching"
          type="search"
          className="pl4 pr3 bn pv2 lh-copy w-100"
        />
      </div>
    </form>
  )
}

export default Search
