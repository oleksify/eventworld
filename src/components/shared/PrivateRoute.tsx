// --- Dependencies
import * as React from 'react'
import { Route, Redirect, RouteProps } from 'react-router-dom'

// --- Hooks
import { useLoggedIn } from '../../store/auth/hook'

/**
 * Component
 */

const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  const isLoggedIn = useLoggedIn()

  return (
    <Route
      exact
      {...rest}
      render={({ location }) =>
        isLoggedIn ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location }
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
