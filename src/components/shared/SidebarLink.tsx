// --- Dependencies
import * as React from 'react'

// --- Components
import Link from './Link'

/**
 * Types
 */

interface Props {
  to: string
  children: React.ReactNode
  isOpen?: boolean
  icon?: React.ReactElement
}

/**
 * Component
 */

const SidebarLink: React.FC<Props> = ({
  to,
  children,
  icon,
  isOpen = false
}) => (
  <Link
    to={to}
    className={`${
      isOpen ? 'ph4 pv3' : 'pa1 mh3 mv2 br3'
    } bg-animate flex items-center f5 light-blue hover-white hover-bg-dark-blue`}
    activeClassName="bg-dark-blue hover-white"
    activeStyle={{ color: 'white' }}
  >
    {icon && (
      <span className={`${isOpen ? 'mr3' : 'mr0'} pa2 flex items-center`}>
        {React.cloneElement(icon, { width: 20, height: 20 })}
      </span>
    )}
    {isOpen && children}
  </Link>
)

export default SidebarLink
