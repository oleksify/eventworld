// --- Dependencies
import * as React from 'react'

// --- Components
import Link from './MainLink'
import LogoutButton from '../elements/LogoutButton'

// --- Hooks
import { useLoggedIn } from '../../store/auth/hook'

/**
 * Component
 */

const MainNav: React.FC = () => {
  const isLoggedIn = useLoggedIn()

  return (
    <nav className="pv4 flex items-center">
      <Link to="/">Home</Link>
      <Link to="/about">About</Link>
      {isLoggedIn ? (
        <>
          <Link to="/admin/dashboard">Dashboard</Link>
          <LogoutButton />
        </>
      ) : (
        <>
          <Link to="/login">Login</Link>
          <Link to="/signup">Sign Up</Link>
        </>
      )}
    </nav>
  )
}

export default MainNav
