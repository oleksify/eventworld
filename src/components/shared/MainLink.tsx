// --- Dependencies
import * as React from 'react'

// --- Components
import Link from './Link'

/**
 * Types
 */

interface Props {
  to: string
  children: React.ReactNode
}

/**
 * Component
 */

const MainLink: React.FC<Props> = ({ to, children }) => (
  <Link
    to={to}
    className="ml4 pv1"
    activeClassName="bb bw1 b--gray"
    activeStyle={{
      color: '#111'
    }}
  >
    {children}
  </Link>
)

export default MainLink
