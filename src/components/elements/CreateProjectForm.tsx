// --- Dependencies
import * as React from 'react'
import { useDispatch } from 'react-redux'
import { useFormik } from 'formik'

// --- Components
import Input from '../shared/Input'
import Label from '../shared/Label'
import Button from '../shared/Button'

// --- Types
import { Dispatch } from '../../store/types'

// --- Actions
import { addProject } from '../../store/projects'

/**
 * Types
 */

interface Props {
  successCallback?: () => void
}

/**
 * Component
 */

const CreateProjectForm: React.FC<Props> = ({ successCallback }) => {
  const dispatch = useDispatch<Dispatch>()
  const formik = useFormik({
    initialValues: {
      title: '',
      description: ''
    },

    onSubmit: async values => {
      formik.setSubmitting(true)
      await dispatch(addProject(values))
      formik.setSubmitting(false)
      successCallback && successCallback()
    }
  })

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="mb3">
        <Label>Title</Label>
        <Input
          type="text"
          id="title"
          placeholder="Project title"
          name="title"
          onChange={formik.handleChange}
          value={formik.values.title}
        />
      </div>

      <div className="mb3">
        <Label>Description</Label>
        <Input
          type="text"
          id="description"
          placeholder="Short description"
          name="description"
          onChange={formik.handleChange}
          value={formik.values.description}
        />
      </div>

      <Button type="submit" disabled={formik.isSubmitting}>
        {formik.isSubmitting ? 'Adding Project...' : 'Create Project'}
      </Button>
    </form>
  )
}

export default CreateProjectForm
