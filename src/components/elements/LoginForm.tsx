// --- Dependencies
import * as React from 'react'
import { useDispatch } from 'react-redux'
import { useFormik } from 'formik'

// --- Icons
import { ReactComponent as IconLogin } from 'feather-icons/dist/icons/log-in.svg'

// --- Types
import { Dispatch } from '../../store/types'

// --- Actions
import { login } from '../../store/auth'

// --- Hooks
import { useAuthErrors } from '../../store/auth/hook'

// --- Components
import Input from '../shared/Input'
import Label from '../shared/Label'
import Button from '../shared/Button'
import Alert, { AlertType } from '../shared/Alert'

/**
 * Component
 */

const LoginForm: React.FC = () => {
  const dispatch = useDispatch<Dispatch>()
  const errors = useAuthErrors()

  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },

    onSubmit: async values => {
      formik.setSubmitting(true)
      await dispatch(login(values))
      formik.setSubmitting(false)
    }
  })

  return (
    <form onSubmit={formik.handleSubmit}>
      {errors && (
        <div className="mb3">
          <Alert type={AlertType.Error}>{errors}</Alert>
        </div>
      )}
      <div className="mb3">
        <Label htmlFor="email">Email:</Label>
        <Input
          id="email"
          placeholder="Your email"
          type="email"
          name="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        />
      </div>
      <div className="mb3">
        <Label htmlFor="password">Password:</Label>
        <Input
          id="password"
          placeholder="Your password"
          type="password"
          name="password"
          onChange={formik.handleChange}
          value={formik.values.password}
        />
      </div>
      <Button type="submit" disabled={formik.isSubmitting}>
        <IconLogin className="white w1 h1 mr2 nl2" />
        Login
      </Button>
    </form>
  )
}

export default LoginForm
