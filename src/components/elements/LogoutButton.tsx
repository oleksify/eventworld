// --- Dependencies
import * as React from 'react'

import { useDispatch } from 'react-redux'

// --- Types
import { Dispatch } from '../../store/types'
import { logout } from '../../store/auth'

// --- Icons
import { ReactComponent as IconLogout } from 'feather-icons/dist/icons/log-out.svg'

/**
 * Component
 */

const Header: React.FC = () => {
  const dispatch = useDispatch<Dispatch>()

  return (
    <button
      className="bg-transparent pointer flex items-center bn ml4 br2 pv2 ph3 link"
      onClick={() => {
        dispatch(logout())
      }}
    >
      <IconLogout className="dark-gray w1 h1" />
    </button>
  )
}

export default Header
