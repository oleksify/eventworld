// --- Dependencies
import * as React from 'react'
import { useDispatch } from 'react-redux'

// --- Types
import { Dispatch } from '../../store/types'

// --- Actions
import { removeTask } from '../../store/tasks'

// --- Hooks
import { useTasks } from '../../store/tasks/hooks'

/**
 * Types
 */

interface Props {
  projectId: number
}

/**
 * Component
 */

const TasksList: React.FC<Props> = ({ projectId }) => {
  const dispatch = useDispatch<Dispatch>()
  const tasks = useTasks(projectId.toString())

  return (
    <>
      {tasks.length > 0 ? (
        <ul className="list pa3 br2 bg-light-gray ma0">
          {tasks.map(task => (
            <li key={task.id} className="">
              <div className="flex items-center justify-between">
                <div>
                  <h5 className="mv0 fw5 f6">{task.title}</h5>
                  <p className="lh-copy f7 pa0 mb0 mt1">{task.description}</p>
                </div>
                <ul className="list pl0 mv0">
                  <li>
                    <button
                      onClick={() => dispatch(removeTask(task.id))}
                      className="br2 bg-transparent pa0 bn red f6 pa2 ttu fw6 pointer hover-bg-red hover-white"
                    >
                      Remove Task
                    </button>
                  </li>
                </ul>
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <p className="lh-copy pa0 tc dark-gray">
          There are no tasks available :( Consider creating one!
        </p>
      )}
    </>
  )
}

export default TasksList
