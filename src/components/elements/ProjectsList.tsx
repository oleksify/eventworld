// --- Dependencies
import * as React from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

// --- Types
import { Dispatch } from '../../store/types'

// --- Actions
import { removeProject } from '../../store/projects'

// --- Hooks
import { useProjects } from '../../store/projects/hooks'

// --- Components
import TasksList from './TasksList'
import CreateTaskForm from './CreateTaskForm'
import Modal from '../shared/Modal'

/**
 * Component
 */

const ProjectsList: React.FC = () => {
  const [isAddingTask, startAddingTask] = React.useState(false)
  const dispatch = useDispatch<Dispatch>()
  const projects = useProjects()

  return (
    <>
      {projects.length > 0 ? (
        <ul className="list pl0 ma0">
          {projects.map(project => (
            <li
              key={project.id}
              className="pa3 ba b--light-gray bg-white br2 mb3"
            >
              <div className="flex items-center justify-between bb b--light-gray pb2">
                <h4 className="mv0 fw6 f5">
                  <Link to={`/admin/projects/${project.id}`}>
                    {project.title}
                  </Link>
                </h4>
                <ul className="list pl0 mv0 flex">
                  <li>
                    <button
                      onClick={() => startAddingTask(true)}
                      className="br2 bg-transparent pa0 bn green f6 pa2 ttu fw6 pointer hover-bg-green hover-white"
                    >
                      Add Task
                    </button>
                    <button
                      onClick={() => dispatch(removeProject(project.id))}
                      className="br2 bg-transparent pa0 bn red f6 pa2 ttu fw6 pointer hover-bg-red hover-white"
                    >
                      Remove Project
                    </button>
                  </li>
                </ul>
              </div>

              <TasksList projectId={project.id} />
              <Modal
                title="Add New Task"
                isOpen={isAddingTask}
                closeFn={() => startAddingTask(false)}
              >
                <CreateTaskForm
                  projectId={project.id}
                  successCallback={() => startAddingTask(false)}
                />
              </Modal>
            </li>
          ))}
        </ul>
      ) : (
        <p className="lh-copy pa0 tc dark-gray">
          There are no projects available :( Consider creating one!
        </p>
      )}
    </>
  )
}

export default ProjectsList
