// --- Dependencies
import * as React from 'react'
import { useDispatch } from 'react-redux'
import { useFormik } from 'formik'

// --- Icons
import { ReactComponent as IconSignUp } from 'feather-icons/dist/icons/user-plus.svg'

// --- Types
import { Dispatch } from '../../store/types'
import { signUp } from '../../store/auth'

// --- Components
import Input from '../shared/Input'
import Label from '../shared/Label'
import Button from '../shared/Button'
import Alert, { AlertType } from '../shared/Alert'

// --- Hooks
import { useAuthErrors } from '../../store/auth/hook'

/**
 * Component
 */

const SignUpForm: React.FC = () => {
  const dispatch = useDispatch<Dispatch>()
  const errors = useAuthErrors()

  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      password: ''
    },

    onSubmit: async values => {
      formik.setSubmitting(true)
      await dispatch(signUp(values))
      formik.setSubmitting(false)
    }
  })

  return (
    <form onSubmit={formik.handleSubmit}>
      {errors && (
        <div className="mb3">
          <Alert type={AlertType.Error}>{errors}</Alert>
        </div>
      )}
      <div className="mb3">
        <Label htmlFor="Name">Name:</Label>
        <Input
          id="name"
          placeholder="Name"
          type="text"
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
        />
      </div>
      <div className="mb3">
        <Label htmlFor="email">Email:</Label>
        <Input
          id="email"
          placeholder="Your email"
          type="email"
          name="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        />
      </div>
      <div className="mb3">
        <Label htmlFor="password">Password:</Label>
        <Input
          id="password"
          placeholder="Your password"
          type="password"
          name="password"
          onChange={formik.handleChange}
          value={formik.values.password}
        />
      </div>

      <Button type="submit" disabled={formik.isSubmitting}>
        <IconSignUp className="white w1 h1 nl2 mr2" />
        Sign Up
      </Button>
    </form>
  )
}

export default SignUpForm
