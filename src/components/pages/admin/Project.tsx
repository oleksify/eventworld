// --- Dependencies
import * as React from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'

// --- Components
import Layout from '../../layouts/Admin'

// --- Actions
import { getProject } from '../../../store/projects'

// --- Types
import { Dispatch } from '../../../store/types'

/**
 * Component
 */

const Project: React.FC = () => {
  const { id } = useParams()
  const dispatch = useDispatch<Dispatch>()

  React.useEffect(() => {
    dispatch(getProject(Number(id)))
  })

  return (
    <Layout pageTitle="Project">
      <div>Project ${id}</div>
    </Layout>
  )
}

export default Project
