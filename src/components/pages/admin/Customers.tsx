// --- Dependencies
import * as React from 'react'

// --- Components
import Layout from '../../layouts/Admin'

/**
 * Component
 */

const Customers: React.FC = () => (
  <Layout pageTitle="Customers">
    <div>Customers</div>
  </Layout>
)

export default Customers
