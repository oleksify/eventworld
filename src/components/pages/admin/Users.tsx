// --- Dependencies
import * as React from 'react'

// --- Components
import Layout from '../../layouts/Admin'

/**
 * Component
 */

const Users: React.FC = () => (
  <Layout pageTitle="Users">
    <div>Users</div>
  </Layout>
)

export default Users
