// --- Dependencies
import * as React from 'react'
import Layout from '../../layouts/Admin'

/**
 * Component
 */

const Dashboard = () => {
  return (
    <Layout pageTitle="Dashboard">
      <div>
        <ul className="list ma0 pl0 flex flex-column flex-row-ns">
          <li className="w-third-ns w-100 pa3 bg-white br3 mr3 grow mb3 mb0-ns">
            <h3 className="f6 fw5 ttu ma0">Projects</h3>
            <p className="f1 fw6 mb0 mt2 pa0 green">23</p>
          </li>

          <li className="w-third-ns w-100 pa3 bg-white br3 mr3 grow mb3 mb0-ns">
            <h3 className="f6 fw5 ttu ma0">Tasks</h3>
            <p className="f1 fw6 mb0 mt2 pa0 green">562</p>
          </li>

          <li className="w-third-ns w-100 pa3 bg-white br3 mr3 grow mb3 mb0-ns">
            <h3 className="f6 fw5 ttu ma0">Customers</h3>
            <p className="f1 fw6 mb0 mt2 pa0 green">12</p>
          </li>
        </ul>
      </div>
    </Layout>
  )
}

export default Dashboard
