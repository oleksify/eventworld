// --- Dependencies
import * as React from 'react'

// --- Components
import Layout from '../../layouts/Admin'

/**
 * Component
 */

const Settings: React.FC = () => (
  <Layout pageTitle="Settings">
    <div>Settings</div>
  </Layout>
)

export default Settings
