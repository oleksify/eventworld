// --- Dependencies
import * as React from 'react'

// --- Components
import Layout from '../../layouts/Admin'

/**
 * Component
 */

const Profile: React.FC = () => (
  <Layout pageTitle="Profile">
    <div>Profile</div>
  </Layout>
)

export default Profile
