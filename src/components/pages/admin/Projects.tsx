// --- Dependencies
import * as React from 'react'
import { useDispatch } from 'react-redux'

// --- Types
import { Dispatch } from '../../../store/types'

// --- Actions
import { getProjects } from '../../../store/projects'
import { getTasks } from '../../../store/tasks'

// --- Icons
import { ReactComponent as IconAdd } from 'feather-icons/dist/icons/plus-circle.svg'

// --- Components
import Button from '../../shared/Button'
import Layout from '../../layouts/Admin'
import ProjectsList from '../../elements/ProjectsList'
import CreateProjectForm from '../../elements/CreateProjectForm'
import Modal from '../../shared/Modal'

/**
 * Component
 */

const Projects = () => {
  const dispatch = useDispatch<Dispatch>()
  const [isAddingProject, startAddingProject] = React.useState(false)

  React.useEffect(() => {
    dispatch(getProjects())
    dispatch(getTasks())
  })

  return (
    <Layout
      pageTitle="Projects"
      actions={
        <Button onClick={() => startAddingProject(true)}>
          <IconAdd className="w1 h1 nl2 mr2" />
          Add Project
        </Button>
      }
    >
      <ProjectsList />
      <Modal
        title="Add New Project"
        isOpen={isAddingProject}
        closeFn={() => startAddingProject(false)}
      >
        <CreateProjectForm successCallback={() => startAddingProject(false)} />
      </Modal>
    </Layout>
  )
}

export default Projects
