// --- Dependencies
import * as React from 'react'

// --- Components
import SignUpForm from '../../elements/SignUpForm'
import Layout from '../../layouts/Site'

/**
 * Component
 */

const Login = () => (
  <Layout>
    <SignUpForm />
  </Layout>
)

export default Login
