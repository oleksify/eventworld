// --- Dependencies
import * as React from 'react'

// --- Components
import Layout from '../../layouts/Site'

/**
 * Component
 */

const About = () => <Layout>About Page</Layout>

export default About
