// --- Dependencies
import * as React from 'react'

// --- Components
import Layout from '../../layouts/Site'

/**
 * Component
 */

const Home = () => <Layout>Home Page</Layout>

export default Home
