// --- Dependencies
import * as React from 'react'

// --- Components
import LoginForm from '../../elements/LoginForm'
import Layout from '../../layouts/Site'

/**
 * Component
 */

const Login = () => (
  <Layout>
    <LoginForm />
  </Layout>
)

export default Login
