export interface Action<Payload> {
  type: string
  payload?: Payload
  error?: boolean
}

export interface Customer {
  id: number
  email: string
  name: string
}

export interface Task {
  id: number
  title: string
  description: string
  projectId: string
}

export interface Project {
  id: number
  title: string
  description: string
  customerId?: number
}

export interface User {
  id: number
  email: string
  password: string
  name?: string
}

export interface Auth {
  isAuthenticated: boolean
  user?: User
  error?: string
}

export interface State {
  projects: Project[]
  customers: Customer[]
  users: User[]
  tasks: Task[]
  auth: Auth
}
