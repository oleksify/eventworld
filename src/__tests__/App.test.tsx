// --- Dependencies
import * as React from 'react'
import { render } from '@testing-library/react'

// --- Components
import App from '../App'

test('renders home page', () => {
  const { getByText } = render(<App />)
  const linkElement = getByText(/home page/i)
  expect(linkElement).toBeInTheDocument()
})
