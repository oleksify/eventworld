# Development Setup

[Open Demo](https://csworld.now.sh)

## To start with one command

1. Install [`overmind`](https://github.com/DarthSim/overmind)
2. `./start.sh`

## To start with separate back/front

1. `yarn server` - should be on port 3000 (or change port in `.env`)
2. `yarn start`
